# tv200
A browser-based open source smart TV platform.

See [architecture.md](architecture.md) for implementation details.

## To install/run
On the device driving your TV clone this repo and run `./start-player.sh` (or manually load the player extension into a Chrome-based browser). Then go to [https://sphi.codeberg.page/tv200](https://sphi.codeberg.page/tv200) from other devices to connect.

## Reset TV's state
If people are connected who you don't want, you don't like your ID or any other reason, you can reset the TV by (on the TV device) pressing escape then enter. All controls will have to manually re-pair.
