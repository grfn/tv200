#!/bin/bash
set -xeuo pipefail

# Get project's root directory
ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"

fetch() {
  curl -L $1 > $2
  sed -i 's://#\s*sourceMappingURL=.*$::g' $2
}

fetch "https://unpkg.com/peerjs/dist/peerjs.min.js" "$ROOT/player/third_party/peerjs.js"
fetch "https://unpkg.com/peerjs/dist/peerjs.min.js" "$ROOT/control/third_party/peerjs.js"
