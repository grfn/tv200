let mediaState = {};
let getAfterProp = () => null;

function setStatus(status) {
  console.log('Status set to', status);
  document.getElementById('status').textContent = status;
}

function showError(message) {
  let elem;
  if (typeof message === 'string') {
    console.error(message);
    elem = document.createElement('p');
    elem.textContent = message;
  } else {
    elem = message;
  }
  document.getElementById('popup-content').appendChild(elem);
  document.getElementById('popup-container').style.display = 'flex';
}

function closePopup() {
  // Clear children so they are not present if the popup is used again
  const content = document.getElementById('popup-content');
  while (content.lastChild) {
    content.removeChild(content.lastChild);
  }
  document.getElementById('popup-container').style.display = 'none'
}

function flashPage() {
  const root = document.getElementById('root');
  root.style.transition = 'all 0s';
  root.classList.add('background-flash');
  setTimeout(() => {
    root.style.transition = 'all 0.5s';
    root.classList.remove('background-flash');
  }, 20);
}

function playMedia(media) {
  console.log('Playing media ' + media);
  if (media) {
    conn.send({
      type: 'enqueue',
      url: media,
      id: generateId(),
      after: getAfterProp(),
    });
    setStatus('Playing');
    document.getElementById('controls').style.display = 'flex';
  } else {
    setStatus('No media');
  }
}

function setMediaInputShown(shown) {
  const mediaInput = document.getElementById('media-input');
  const mediaButtons = document.getElementById('media-buttons');
  mediaInput.style.display = shown ? 'block' : 'none';
  mediaButtons.style.display = shown ? 'none' : 'block';
  if (shown) {
    // On mobile, giving something to select brings up the paste menu
    mediaInput.value = mediaInput.placeholder;
    mediaInput.focus();
    mediaInput.select();
  } else {
    mediaInput.value = '';
    mediaInput.blur();
  }
}

{
  const mediaInput = document.getElementById('media-input');
  mediaInput.addEventListener('focusout', () => {
    setMediaInputShown(false);
  });
  mediaInput.addEventListener('paste', ev => {
    const text = ev.clipboardData.getData('Text');
    playMedia(text);
    flashPage();
    setMediaInputShown(false);
  });
}

function onPlayNowClicked() {
  getAfterProp = () => null;
  setMediaInputShown(true);
}

function onPlayNextClicked() {
  getAfterProp = () => mediaState.queue.length ? mediaState.queue[0].id : null;
  setMediaInputShown(true);
}

function onAddToQueueClicked() {
  getAfterProp = () => mediaState.queue.length ? mediaState.queue[mediaState.queue.length - 1].id : null;
  setMediaInputShown(true);
}

function onCloseMediaClicked() {
  const ids = mediaState.queue.map(media => media.id);
  conn.send({
    type: 'drop-media',
    ids: ids,
  });
  console.log('dropping IDs:', ids);
  setStatus('Closed');
  flashPage();
}

function requestSeekTo(seconds) {
  conn.send({
    type: 'action',
    action: 'seek',
    time: seconds,
  });
}

function onPlayPauseClicked() {
  conn.send({
    type: 'action',
    action: 'toggle-play',
  });
}

function onSkipPrevClicked() {
  conn.send({
    type: 'action',
    action: 'prev',
  });
}

function onSkipNextClicked() {
  conn.send({
    type: 'action',
    action: 'next',
  });
}

function updateConnectedState(connected) {
  function setDisplay(className, display) {
    const collection = document.getElementsByClassName(className);
    for (let i = 0; i < collection.length; i++) {
      collection[i].style.display = display ? 'inline' : 'none';
    }
  }
  setDisplay("only-when-connected", connected);
  setDisplay("only-when-disconnected", !connected);
  document.getElementById('content').style.display = connected ? 'block' : 'none';
  const inputElem = document.getElementById('peer-id-input');
  inputElem.disabled = connected;
  if (connected) {
    inputElem.blur();
  }
  if (connected) {
    inputElem.classList.add('peer-id-display');
  } else {
    inputElem.classList.remove('peer-id-display');
  }
}

let resettingState = false;
function resetState(autoconnect, status) {
  if (resettingState) {
    return;
    console.log('Not resetting state because reset already in-progress');
  }
  setStatus(status ? status : (navigator.onLine ? 'Not connected' : 'Waiting for network...'));
  console.log('Resetting state...');
  resettingState = true;
  try {
    if (peer && !peer.destroyed) {
      peer.destroy();
    }
    if (conn && conn.open) {
      conn.close();
    }
    peer = null;
    conn = null;
    reopenOnError = false;
    updateConnectedState(false);
    setMediaInputShown(false);
    mediaState = {
      queue: [],
      playing: false,
      length: null,
      progress: null,
      speed: 1.0,
      actions: [],
    };
    if (autoconnect) {
      tryAutoconnect();
    }
  } catch (e) {
    console.log(e);
  }
  resettingState = false;
}

function focusIdInput() {
  const elem = document.getElementById('peer-id-input');
  elem.setSelectionRange(elem.value.length, elem.value.length);
  elem.focus();
}

function createQueueElem() {
  const template = document.getElementById('queue-item');
  return template.content.cloneNode(true);
}

function initQueueElem(elem, item) {
  getDescendantById(elem, 'queue-item-name').textContent = item.name ? item.name : item.url;
  // NOTE: we need to use .onclick instead of .addEventListener() so that stale listeners don't
  // stick around when an item is reused
  getDescendantById(elem, 'queue-item-remove').onclick = () => {
    conn.send({
      type: 'drop-media',
      ids: [item.id],
    });
  };
  getDescendantById(elem, 'queue-item-play').onclick = () => {
    conn.send({
      type: 'move-media',
      id: item.id,
      after: null,
    });
  };
}

function updateQueue(queue) {
  const queueElem = document.getElementById('queue-div');
  while (queueElem.children.length < queue.length) {
    queueElem.appendChild(createQueueElem())
  }
  while (queueElem.children.length > queue.length) {
    queueElem.removeChild(queueElem.lastChild);
  }
  for (let i = 0; i < queue.length; i++) {
    initQueueElem(queueElem.children[i], queue[i]);
  }
}

function handleState(message) {
  Object.keys(message).forEach(key => {
    mediaState[key] = message[key];
  });
  if (hasAny(message, ['queue'])) {
    updateQueue(message.queue);
  }
  if (hasAny(message, ['queue', 'playing'])) {
    if (mediaState.queue.length) {
      if (mediaState.playing) {
        setStatus('Playing ' + mediaState.queue[0].name);
      } else {
        setStatus('Paused ' + mediaState.queue[0].name);
      }
    } else {
      setStatus('Ready');
    }
  }
  if (hasAny(message, ['playing', 'length', 'progress', 'speed'])) {
    if (mediaState.length !== null && mediaState.progress !== null) {
      setProgress(
        mediaState.progress / mediaState.speed,
        mediaState.length / mediaState.speed,
        mediaState.playing,
      );
    } else {
      setProgress(0, 1, false);
    }
  }
}

function handleMessage(message) {
  playerIsAlive();
  if (message.type === 'state') {
    handleState(message);
  } else if (message.type === 'ping') {
    console.log('Ponging');
    conn.send({type: 'pong'});
  } else if (message.type === 'pong') {
    // Ignore
  } else {
    showError('message has unknown type: ' + JSON.stringify(message));
  }
}

function onConnectClicked() {
  const pairingId = document.getElementById('peer-id-input').value.toUpperCase();
  tryPair(pairingId);
}

function onDisconnectClicked() {
  resetState(false);
}

// If the user types in an upper case letter in the pairing code field, remind them it is not case
// sensitive
document.getElementById('peer-id-input').addEventListener('input', e => {
  if (e.data && e.data.toLowerCase() !== e.data) {
    setStatus('Not case sensitive');
  }
});

resetState(true, 'Initializing...');
