const idLength = 8;

function hasAny(obj, props) {
  for (let i = 0; i < props.length; i++) {
    if (obj[props[i]] !== undefined) {
      return true;
    }
  }
  return false;
}

function generateId() {
  result = '';
  for (let i = 0; i < idLength; i++) {
    const num = Math.floor(Math.random() * 52);
    if (num >= 26) {
      result += String.fromCharCode(num - 26 + 97);
    } else {
      result += String.fromCharCode(num + 65);
    }
  }
  return result;
}

// Recursively searches element tree, returns first element that matches or null
function getDescendant(elem, pred) {
  if (pred(elem)) {
    return elem;
  }
  for (let i = 0; i < elem.children.length; i++) {
    const result = getDescendant(elem.children[i], pred);
    if (result !== null) {
      return result;
    }
  }
  return null;
}

function getDescendantById(elem, id) {
  return getDescendant(elem, e => e.id === id);
}
