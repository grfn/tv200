console.log('Hello from content script!');

let fadeDiv = null;
let manager = null;

function showFadeDiv() {
  const color = '#303030';

  fadeDiv = document.createElement('div');
  fadeDiv.style.top = '0';
  fadeDiv.style.left = '0';
  fadeDiv.style.position = 'fixed';
  fadeDiv.style.width = '100%';
  fadeDiv.style.height = '100%';
  fadeDiv.style.zIndex = '10000';
  fadeDiv.style.background = color;
  fadeDiv.style.pointerEvents = 'none';
  document.documentElement.appendChild(fadeDiv);
}

function fadeIn() {
  const timeout = 1;

  fadeDiv.style.opacity = '0';
  fadeDiv.style.transition = 'opacity ' + timeout + 's';
  setTimeout(() => {
    if (document.documentElement.contains(fadeDiv)) {
      document.documentElement.removeChild(fadeDiv);
      fadeDiv = null;
    }
  }, timeout * 1000 + 100);
}

function removeElementsExcept(elem, toKeep) {
  let i = 0;
  while (i < elem.children.length) {
    if (toKeep.includes(elem.children[i])) {
      i++;
    } else {
      elem.children[i].remove();
    }
  }
}

function hideAllButTree(elem) {
  while (elem != document.documentElement) {
    elem.style.display = '';
    const toKeep = elem;
    elem = elem.parentNode;
    elem.style.overflow = 'hidden';
    for (let i = 0; i < elem.children.length; i++) {
      const child = elem.children[i];
      if (child !== toKeep && child.nodeType === Node.ELEMENT_NODE) {
        child.style.display = 'none';
      }
    }
  }
}

function showElementBig(elem) {
  elem.style.top = '0';
  elem.style.left = '0';
  elem.style.position = 'fixed';
  elem.style.width = '100%';
  elem.style.height = '100%';
  elem.style.background = 'black';
  elem.style.zIndex = '10000';
}

function waitFor(selector, callback) {
  const waitTime = 30;
  const interval = 0.2;
  const maxAttempts = waitTime / interval;
  let attempts = 0;
  function iteration() {
    const elem = document.querySelector(selector);
    if (elem) {
      callback(elem);
    } else if (attempts > maxAttempts) {
      console.log('could not find ' + selector + ' element in page');
    } else {
      attempts += 1;
      setTimeout(iteration, interval * 1000);
    }
  }
  iteration();
}

function clickOn(selector) {
  const elem = document.querySelector(selector);
  if (elem) {
    elem.click();
    return true;
  } else {
    console.error('could not find element ' + selector);
    return false;
  }
}

function trackVideoState(video) {
  function sendState(message) {
    message.type = 'state';
    chrome.runtime.sendMessage(message);
  }
  let timeout = null;
  function sendProgress() {
    clearTimeout(timeout);
    const message = {
      playing: !video.paused,
      length: video.duration ? video.duration : null,
      progress: video.currentTime,
    };
    sendState(message);
    if (video.playing) {
      timeout = setTimeout(sendProgress, 10000);
    }
  }
  sendState({
    playing: !video.paused,
    length: video.duration ? video.duration : null,
    progress: video.currentTime,
    speed: video.playbackRate,
    actions: ['toggle-play', 'seek'],
  });
  if (video.playing) {
    timeout = setTimeout(sendProgress, 1000);
  }
  video.addEventListener('play', sendProgress);
  video.addEventListener('pause', sendProgress);
  video.addEventListener('seeked', sendProgress);
  video.addEventListener('durationchange', () => {
    sendState({length: video.duration});
  });
  video.addEventListener('ratechange', () => {
    sendState({speed: video.playbackRate});
  });
}

class GenericVideo {
  constructor() {
    this.video = null;
    waitFor('video', videoElem => {
      this.video = videoElem;
      trackVideoState(this.video);
      this.video.play();
      this.video.style.cursor = 'none';
      // Needs user interaction on Chrome :(
      //this.video.requestFullscreen();
      showElementBig(this.video);
      hideAllButTree(this.video);
    });
  }

  togglePlay() {
    if (this.video) {
      if (this.video.paused || this.video.ended) {
        this.video.play();
      } else {
        this.video.pause();
      }
    }
  }

  seekTo(time) {
    this.video.currentTime = time;
  }
}

class Youtube {
  isInTheaterMode() {
    // Return true the element with ID 'player-theater-container' has any children
    return document.querySelector('#player-theater-container > *');
  }

  constructor() {
    this.video = null;
    document.documentElement.style.overflow = 'hidden';

    this.observer = new MutationObserver((_mutations, _observer) => {
      showElementBig(this.video);
    });

    waitFor('video', elem => {
      this.video = elem;
      trackVideoState(this.video);
      showElementBig(this.video);
      this.observer.observe(this.video, {
        attributes: true,
        attributeFilter: ['style'],
      });

      let skeletonPlayer = document.querySelector('#player');
      if (skeletonPlayer) {
        showElementBig(skeletonPlayer);
        // We don't want to hide all but the skeleton because then switching to theater mode will end up with wrong sized
        // controls for some reason.
      }

      this.video.play();
      waitFor('#ytd-player', player => {
        // Youtube has two modes, the normal mode with recommended videos on the right and a "theater" mode where the
        // main video is bigger. The controls refuse to size properly unless in theater mode. The theater player is in
        // #player-theater-container and the non-theater player is in #player-container-inner. Both exist in the other
        // mode, but if they are not active they do not have children. We use this property to determine which mode
        // we are in.
        if (document.querySelector('#player-container-inner > *')) {
          // If we are in normal mode, wait three seconds and then press the button to switch to
          // theater mode (pressing immediately doesn't work for some reason, and it's not clear
          // what we're waiting for)
          setTimeout(() => {
            clickOn('.ytp-size-button');
          }, 3000);
        };
        // When the theater container gets children (either immediately, or after switching to theater mode) insert the
        // video back where youtube expects it.
        waitFor('#player-theater-container > *', () => {
          showElementBig(this.video);
          showElementBig(player);
          hideAllButTree(player);
        });
      });
    });
  }

  togglePlay() {
    if (this.video) {
      if (this.video.paused || this.video.ended) {
        this.video.play();
      } else {
        this.video.pause();
      }
    }
  }

  seekTo(time) {
    this.video.currentTime = time;
  }

  skipNext() {
    if (this.video) {
      this.video.currentTime += 10;
    }
  }

  skipPrev() {
    if (this.video) {
      this.video.currentTime -= 10;
    }
  }
}

class YtMusic {
  constructor() {
    this.video = null;
    waitFor('video', elem => {
      this.video = elem;
      trackVideoState(this.video);
    });
    if (window.location.pathname === '/playlist') {
      clickOn('.ytmusic-section-list-renderer .title a');
    }
  }

  togglePlay() {
    if (this.video) {
      if (this.video.paused || this.video.ended) {
        this.video.play();
      } else {
        this.video.pause();
      }
    }
  }

  seekTo(time) {
    this.video.currentTime = time;
  }

  skipNext() {
    clickOn('.next-button');
  }

  skipPrev() {
    clickOn('.previous-button');
  }
}

class Pandora {
  constructor() {
  }

  togglePlay() {
    clickOn('.PlayButton');
  }

  skipNext() {
    clickOn('.SkipButton');
  }
}

function handleMessage(message) {
  if (message.type === 'action') {
    if (message.action === 'toggle-play') {
      manager.togglePlay();
    } else if (message.action === 'next') {
      manager.skipNext();
    } else if (message.action === 'prev') {
      manager.skipPrev();
    } else if (message.action === 'seek') {
      manager.seekTo(message.time);
    } else {
      console.error('Unknown action ' + message.action);
    }
  } else if (message.type === 'prepare-close') {
    // Removing all elements prevents the annoying close tab confirmations
    showFadeDiv();
    removeElementsExcept(document.documentElement, [fadeDiv]);
  } else {
    console.error('Unknown message type: ' + JSON.stringify(message));
  }
}

function initManager() {
  const hostname = window.location.hostname;
  if (hostname.endsWith('pandora.com')) {
    manager = new Pandora();
  } else if (hostname.endsWith('music.youtube.com')) {
    manager = new YtMusic();
  } else if (hostname.endsWith('youtube.com')) {
    manager = new Youtube();
  } else {
    manager = new GenericVideo();
  }
}

chrome.runtime.onMessage.addListener(
  (message, _sender, _sendResponse) => {
    console.log('Content got message: ', JSON.stringify(message));
    handleMessage(message);
  }
);

window.addEventListener('load', () => {
  chrome.runtime.sendMessage({
    type: 'ready',
  }, () => {
    fadeIn();
  });
});

// Can't hurt, prevents some of the close tab confirmations (but has no effect against the addEventListener() ones)
window.onbeforeunload = null;

showFadeDiv();
initManager();

if (location.hostname.endsWith('southwest.com')) {
  setTimeout(() => {
    location.reload();
  }, 12000 + Math.random() * 5000);
}
