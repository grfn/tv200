const errorTimeout = 7000;
// The current state of the actual media tab, which may be briefly out of step with the queue
let currentMedia = {
  tabId: null,
  mediaId: null,
  mediaName: null,
}
let lastProgressTime = 0;
let mediaIdChars = 8;
let mediaState = {};

function updateMediaState(state) {
  // If the current media ID according to the new state's queue doesn't match the actual current
  // media ID
  if (state.queue !== undefined)
  {
    state.queue = [...state.queue]; // clone
    currentMedia.mediaName = state.queue.length ? state.queue[0].name : null
    if (state.queue.length && state.queue[0].id !== currentMedia.mediaId) {
      // We need to play a new media
      currentMedia.mediaId = state.queue[0].id;
      const mediaUrl = state.queue[0].url
      if (currentMedia.tabId) {
        chrome.tabs.sendMessage(currentMedia.tabId, {
          type: 'prepare-close',
        });
      }
      currentMedia.tabId = null;
      chrome.runtime.sendMessage({
        type: 'open',
        url: state.queue[0].url,
      });
    } else if (!state.queue.length) {
      // We need to close any existing media tabs and null out the rest of the state
      currentMedia.mediaId = null;
      if (currentMedia.tabId) {
        chrome.tabs.sendMessage(currentMedia.tabId, {
          type: 'prepare-close',
        });
      }
      currentMedia.tabId = null;
      chrome.runtime.sendMessage({type: 'close'});
      state = {
        queue: [],
        playing: false,
        length: null,
        progress: null,
        speed: 1.0,
        actions: [],
      }
    }
  }
  let send = false;
  let message = {
    type: 'state',
  };
  Object.keys(state).forEach(key => {
    if (state[key] !== mediaState[key]) {
      send = true;
      mediaState[key] = state[key];
      message[key] = state[key];
    }
  });
  if (state.progress) {
    lastProgressTime = performance.now();
  }
  if (send) {
    console.log('sending state to controls:', message);
    sendToAllControls(message);
  }
  if (mediaState.queue.length > 0 &&
      !mediaState.playing &&
      mediaState.progress &&
      mediaState.length &&
      mediaState.progress > mediaState.length - 0.1
  ) {
    mediaState.queue.splice(0, 1);
    updateMediaState({
      queue: mediaState.queue,
      length: null,
    });
  }
  updateStatus();
}

function getMediaState() {
  if (mediaState.progress !== null && mediaState.playing) {
    const currentTime = performance.now();
    const deltaMs = currentTime - lastProgressTime;
    mediaState.progress += deltaMs * mediaState.speed / 1000;
    lastProgressTime = currentTime;
  }
  mediaState.type = 'state'
  return mediaState;
}

function showError(message) {
  const elem = document.getElementById('errors');
  const p = document.createElement('p');
  p.textContent = message;
  elem.appendChild(p);
  setTimeout(() => {
    try {
      elem.removeChild(p);
    } catch {}
  }, errorTimeout);
}

if (typeof chrome === 'undefined' || !chrome.runtime) {
  showError('Home page not connected to extension');
}

function setStatus(status) {
  document.getElementById('status').textContent = status;
}

function updateStatus() {
  if (currentMedia.mediaId) {
    if (currentMedia.mediaName) {
      setStatus('Playing ' + currentMedia.mediaName);
    } else {
      setStatus('Loading...');
    }
  } else if (conns.length === 0) {
    setStatus('Waiting');
  } else {
    setStatus('Ready');
  }
}

function updateConnCount() {
  const elem = document.getElementById('connected-devices');
  if (conns.length === 0) {
    elem.textContent = 'no connected devices';
  } else if (conns.length === 1) {
    elem.textContent = '1 connected device';
  } else {
    elem.textContent = conns.length + ' connected devices';
  }
}

// Listen for messages from the content and background scripts
chrome.runtime.onMessage.addListener(
  (message, sender, _sendResponse) => {
    console.log('Home got message:', message);
    if (message.type === 'ready') {
      currentMedia.tabId = sender.tab.id;
    } else if (message.type === 'state') {
      updateMediaState(message);
    } else {
      console.error('Unknown message type: ' + JSON.stringify(message));
    }
  }
);

function sendToAllControls(message) {
  for (let i = 0; i < conns.length; i++) {
    conns[i].send(message);
  }
}

function domainRegex(domain) {
  return new RegExp('(http(s)?://)?((^/)*\\.)?' + domain + '($|/)');
}
const knownNames = new Map();
const titleRegex = new RegExp('<title>(.*)</title>');
const youtubeRegex = domainRegex('youtube.com');
const shortYoutubeRegex = domainRegex('youtu.be');
function withTitleForUrl(url, callback) {
  if (knownNames.has(url)) {
    callback(knownNames.get(url));
    return;
  }
  let handleSuccess;
  let requestUrl;
  if (url.match(youtubeRegex) || url.match(shortYoutubeRegex)) {
    requestUrl = 'https://www.youtube.com/oembed?url=' + encodeURIComponent(url) + '&format=json';
    handleSuccess = content => {
      metadata = JSON.parse(content);
      return metadata.title;
    }
  } else {
    requestUrl = url;
    handleSuccess = content => {
      return content.match(titleRegex)[1];
    }
  }
  const reportError = message => {
    showError('Could not get title for ' + url + ': ' + message);
    knownNames.set(url, url);
    callback(url);
  }
  const request = new XMLHttpRequest();
  request.open('GET', requestUrl);
  request.onload = () => {
    if (request.status === 200) {
      try {
        const name = handleSuccess(request.responseText);
        knownNames.set(url, name);
        callback(name);
      } catch (error) {
        reportError(error);
      }
    } else {
      reportError('code ' + request.status);
    }
  }
  request.onerror = evt => {
    reportError(evt);
  }
  request.send();
}

function addMediaToQueue(media, after) {
  if (after === null) {
    if (mediaState.queue.length) {
      mediaState.queue[0] = media;
    } else {
      mediaState.queue.push(media);
    }
  } else {
    const index = mediaState.queue.findIndex(media => media.id == after);
    mediaState.queue.splice(index < 0 ? Infinity : index + 1, 0, media);
  }
}

function removeMediaFromQueue(mediaId) {
  const index = mediaState.queue.findIndex(media => media.id === mediaId);
  if (index < 0) {
    console.log('could not find item to drop from queue with ID', mediaId);
    return null;
  } else {
    console.log('dropping item at queue position', index, 'with ID', mediaId);
    return mediaState.queue.splice(index, 1)[0];
  }
}

// Called when we get a message from a Control device
function handleControlMessage(conn, message) {
  if (message.type === 'enqueue') {
    addMediaToQueue({
      url: message.url,
      id: message.id,
      name: null
    }, message.after);
    updateMediaState({queue: mediaState.queue});
    withTitleForUrl(message.url, name => {
      for (let i = 0; i < mediaState.queue.length; i++) {
        if (mediaState.queue[i].id === message.id) {
          mediaState.queue[i].name = name;
        }
      }
      updateMediaState({queue: mediaState.queue});
    });
  } else if (message.type === 'move-media') {
    const media = removeMediaFromQueue(message.id);
    if (media) {
      addMediaToQueue(media, message.after);
    }
    updateMediaState({queue: mediaState.queue});
  } else if (message.type === 'drop-media') {
    for (let i = 0; i < message.ids.length; i++) {
      removeMediaFromQueue(message.ids[i]);
    }
    updateMediaState({queue: mediaState.queue});
  } else if (message.type === 'action') {
    if (currentMedia.tabId) {
      chrome.tabs.sendMessage(currentMedia.tabId, message);
    } else {
      showError('No media tab to control');
    }
  } else if (message.type === 'ping') {
    conn.send({type: 'pong'});
  } else if (message.type === 'pong') {
    // ignore
  } else {
    showError('Message has invalid type: ' + JSON.stringify(message))
  }
}

pendingKeyCommand = false;
pendingKeyCommandTimeout = null;
document.addEventListener('keydown', event => {
  if (event.key === 'Escape') {
    showError('now press R to reset or Q to quit to desktop');
    clearTimeout(pendingKeyCommandTimeout);
    pendingKeyCommandTimeout = setTimeout(() => {
      pendingKeyCommand = false;
    }, errorTimeout);
    pendingKeyCommand = true;
  } else if (pendingKeyCommand) {
    clearTimeout(pendingKeyCommandTimeout);
    pendingKeyCommand = false;
    if (event.key === 'r') {
      resetNetworkId();
    } else if (event.key === 'q') {
      showError('Quitting...');
      chrome.runtime.sendMessage({
        type: 'quit',
      });
    } else {
      showError('Unknown command');
    }
  }
}, true);

updateMediaState({queue: []});
setStatus('Initializing...');
updateConnCount();
cleanOldSessions(() => {
  initPeer();
});
